## 文档 

/help 显示此文档

code值说明：
{
    1：suc,
    2: new,
    0: not existed,
    -1: err format
    -2: tower ac
}

## 战场

/battle/lefttime 剩余时间 <num>

/battle/bounds 边界值 ex:[px,py,qx,qy]

## 用户

### /user/info 人物信息

post
{
    user_id:<str>
} -> {
    code : <num>
    name:<str>
    position:[px, py],
    resource:<num>,
    towers:[<str>]
}


### /user/update  更新人物信息

post
{
    user_id:<str>
    name:<str>
    position:[px, py],  //avail
    resource:<num>,     //avail 
    towers:[<str>],     //avail
}
} -> {
    code:<num>
}


### /user/list 所有用户

get

[</user/info>]

### /user/rank 获取排名

get
[
    {
        id:<str>,
        name:<str>,
        rank:<num>,
        score:<num>
    }
]


## 圆塔


### /tower/list 获取所有塔

get

[</tower/info>]


### /tower/info 获取塔信息

post
{
    tower_id:<str>
} -> {
    code: <num>
    position:[px,py],
    resource:<num>,
    tower_state:<str:out_ctrl/on_fire/on_picking>,
    owner: // or null
    {
        user_id:<str>,
        user_name:<str>,
        picking_rate:<num>,
        defend:<num>,
    }
}


### /tower/control 尝试控制

post
{
    user_id:<str>,
    tower_id:<str>
} -> {
    code: <num>
}

### /tower/attack 尝试进攻

post
{
    user_id:<str>,
    tower_id:<str>,
    attack_res:<num>
} -> {
    code: <num>
}

### /tower/defend 部署防御

post
{
    user_id:<str>,
    tower_id:<str>,
    defend_res:<num>
    tech:<str:auto_picking/sheild/smoke> //avail
} -> {
    code: <num>
}


## 奖励任务

### /task/cd

{
    user_id:<str>,
} -> {
    cds:<[num]>
}

### /task/complete

{
    user_id:<str>,
    task_no:<num>,
    set_reset_time:<num>(default:24h)
} -> {
    code:<num> 
}


