var express = require('express');
var router = express.Router();

/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('index', { title: 'Express' });
});


/*********************** data  ***************************/

const TOWER_STATE = {
  OUT_CTRL:   "out_ctrl",
  ON_FIRE:    "on_fire",
  ON_PICKING: "on_picking"
}
const TEST_AREA = [
  120.3390026093, 30.3183216427, 
  120.3483581543, 30.3100600394,
];

const TECH_TREE = {
  AUTO_PICKING: 'auto_picking',
  SHEILD: 'sheild',
  SMOKE: 'smoke'
}
const NOR_PICKING_RATE = 10;
const AUTO_PICKING_EXTRA = 2;
const ATK_DEF_RATE = 1.2;
const SHEILD_RATE = 1.5;

const GENERATE_N = 1.5
const GENERATE_M = 1.5



const TOTAL_TIME = 3 * 3600;
var LEFT_TIME = TOTAL_TIME;

var grid = []
for(var i = 0;i<64;i++) {
  grid[i] = new Unit()
}

/*********************** struct ***************************/
function getRandomPosition(area){
  var x = Math.random() * (area[2] - area[0]) + area[0];
  var y = Math.random() * (area[3] - area[1]) + area[1];
  return [Number(x.toFixed(10)), Number(y.toFixed(10))];
};

function calculatorLoss(atk, def, rate){
  var win = Math.round(atk - def * rate); 
  if (win > 0)
    return win;
  else
    return Math.round(atk / rate - def);
}

function Unit(){
  this.rou = 1;
  this.flow = 1;
  this.number = 0;
  this.old_owners = {};
  this.addTower = () => {
    this.number++
  }
  this.subTower = () => {
    this.number--
  }
  this.weight = ()=>{
    return this.number*(GENERATE_M*this.row/this.flow + GENERATE_N*this.flow/this.row)
  }
  this.ownerChange = oldOwner => {
    this.old_owners[oldOwner] = true
    this.flow = Object.keys(this.old_owners)
    this.rou++
  }
}

Unit.CalculateIndex = (LULnglat, RDLnglat, regionNum, tower) => {
  let dHeight = Math.abs(RDLnglat[1] - LULnglat[1])
  let dWidth =  Math.abs(RDLnglat[0] - LULnglat[0])

  let length = Math.floor(Math.sqrt(regionNum))

  let unitHeight = dHeight / length
  let unitWidth = dWidth / length

  let i = Math.floor(Math.abs(tower.position[0] - LULnglat[0]) / length)
  let j = Math.floor(Math.abs(tower.position[1] - LULnglat[1]) / length)

  return j*length + i
}
Unit.CalculateRegion = (LULnglat, RDLnglat, regionNum, index) => {
  let dHeight = RDLnglat[1] - LULnglat[1]
  let dWidth =  RDLnglat[0] - LULnglat[0]

  let length = Math.floor(Math.sqrt(regionNum))

  let unitHeight = dHeight / length
  let unitWidth = dWidth / length

  return [
    LULnglat[0] + index % 8 * unitWidth,      LULnglat[1] + index / 8 * unitHeight,
    LULnglat[0] + (index % 8 + 1) * unitWidth, LULnglat[1] + (index / 8 + 1) * unitHeight
  ]
}

function TC(){
  this.TOWERS = [];
  this.create = (times)=>{
    for(var i=0; i<times; i++){
      var t = new Tower();
      t.owner = null;
      t.id = this.getID();
      t.position = getRandomPosition(TEST_AREA);
      this.add(t);
    }
  };
  this.createATower = position => {
    var t = new Tower();
    t.owner = null;
    t.id = this.getID();
    t.position = position;
    this.add(t);
  }
  this.filter = (t_id) =>{
    return this.TOWERS.filter((i)=>{
      return i.id == t_id
    })[0];
  };
  this.getID = ()=>{
    return Date.parse(new Date()) - Math.round(Math.random()* 100000);
  }
  this.add = (t)=>{
    let i = Unit.CalculateIndex(TEST_AREA.slice(0, 2), TEST_AREA.slice(2), 64, t)
    grid[i].addTower()
    this.TOWERS.push(t);
  }
  this.remove = (t)=>{
    let i = Unit.CalculateIndex(TEST_AREA.slice(0, 2), TEST_AREA.slice(2), 64, t)
    grid[i].subTower()

    for (i in this.TOWERS){
      if (t == this.TOWERS[i]) {
        if (t.owner) {
          var u = UserController.filter(t.owner.user_id);
          if (u) u.lose_tower(t);
          t.owner = null;
          t.state = TOWER_STATE.OUT_CTRL;
        }
        this.TOWERS.splice(i, 1);
        this.create(1);
        break;
      }
    }
  }
  //this.generate = () => {
    //if(this.TOWERS.length >= 10) return

    //let minIndex = Math.floor(Math.random() * Math.floor(grid.length))
    //let minWeight = grid[minIndex].weight()
    //for(let i = 0;i<grid.length;i++) {
      //if(grid[i].weight() > minWeight) {
        //minWeight = grid[i].weight()
        //minIndex = i
      //}
    //}
    //let r = Unit.CalculateRegion(TEST_AREA.slice(0, 2), TEST_AREA.slice(2), 64, minIndex)

    //this.createATower(getRandomPosition(r))

    //setTimeout(this.generate, 2000)
  //}
}

const NAME = ['xana', 'mephis', 'sjj', 'hellovass', 'juryxiong'];

function UC(){
  this.USERS = [];
  this.create = (times)=>{
    for (var i = 0; i<times;i++){
      var u = new User();
      u.name = NAME[i];
      u.id = "ROBOT" + Math.round(Math.random() * 10000);
      u.position = getRandomPosition(TEST_AREA);
      u.resource = 500 + Math.round(Math.random() * 3);
      this.add(u);
    }
  }
  this.filter = (u_id) =>{
    return this.USERS.filter((i)=>{
      return i.id == u_id}
    )[0];
  };
  this.add = (u)=>{
    this.USERS.push(u);
  };
  this.update = (u_id, data)=>{
    var u = this.filter(u_id);
    var code = u==undefined?2:1;
    if(code==2) {
      u = new User();
      u.id = data.user_id;
    }

    if (data.name) u.name = data.name;
    if (data.position) u.position = data.position;
    if (data.resource) u.resource = data.resource;
    if (data.towers) u.towers = data.towers;
    
    if(code==2) this.add(u);
    return code;
  };
  this.sort = () =>{
    return this.USERS.sort((a, b)=>{
      return a.resource < b.resource;
    });
  }
  this.ranklist = () => {
    var arr = [];
    var src = this.sort();
    var score = src[0].resource;
    var rank = 1;
    for (var i in src){
      if (src[i].resource < score) rank = Number(i)+1;
      arr.push({
        id:src[i].id,
        name:src[i].name,
        score:src[i].resource,
        rank:rank
      });
      score = src[i].resource;
    }
    return arr;
  }
}

function Tower() {
  this.id;
  this.position;
  this.resource = 18000;
  this.state = TOWER_STATE.OUT_CTRL;
  this.owner = {
    user_id:null,
    user_name:null,
    picking_rate:NOR_PICKING_RATE,
    defend:0
  }
  this.tech = null;
  this.spell_tech = (tc)=>{
    var tech_allow = false;
    for (var k in TECH_TREE) {
      if (tc == TECH_TREE[k]) { tech_allow = true;};
    }
    if (!tech_allow) return false;
    this.tech = tc;
    if(tc == TECH_TREE.AUTO_PICKING && this.owner){
        this.owner.picking_rate = NOR_PICKING_RATE + AUTO_PICKING_EXTRA;
    }
    return true;
  }
  this.lose_tech = ()=>{
    if (this.owner)
        this.owner.picking_rate = NOR_PICKING_RATE;
    this.tech = null;
  }
}

function User() {
  this.id;
  this.name = 'noname';
  this.position = [
    TEST_AREA[0]/2+TEST_AREA[2]/2,
    TEST_AREA[1]/2+TEST_AREA[3]/2,
  ];
  this.resource = 500;
  this.towers = [];
  this.capture_tower = (t)=>{
    var i = this.towers.indexOf(t.id);
    if (i==-1)
      this.towers.push(t.id);
  }
  this.lose_tower = (t)=>{
    var i = this.towers.indexOf(t.id);
    if(i!=-1)
      this.towers.splice(i,1);
  }
  this.gather_resource = ()=>{
    for (var i in this.towers){
      var t = TowerController.filter(this.towers[i]);
      if (!t) continue;
      var pr = t.owner.picking_rate;
      if (t.resource > 0) {
        t.resource -= pr;
        this.resource += pr;
      } else {
        TowerController.remove(t);
      }
    }
  }
}

/********************** test data **************************/

var TowerController = new TC();
var UserController = new UC();

TowerController.create(5);
//TowerController.generate();
UserController.create(5);

  //120.3390026093, 30.3183216427, 
  //120.3483581543, 30.3100600394,
var ffff = new Tower();
ffff.position = [120.3469980000,30.3118010000]
ffff.id = 32323;
ffff.owner = null;
TowerController.add(ffff);


for (var i =0;i<2;i++){
  var u = UserController.USERS[0];
  var t = TowerController.TOWERS[i];
  t.owner = {
    user_id: u.id,
    user_name: u.name,
    picking_rate: 10,
    defend: 100
  };
  u.capture_tower(t);
  t.state = TOWER_STATE.ON_PICKING;
}

for (var i =2;i<3;i++){
  var u = UserController.USERS[1];
  var t = TowerController.TOWERS[i];
  t.position = [120.3459890000,30.3118110000]
  t.owner = {
    user_id: u.id,
    user_name: u.name,
    picking_rate: 10,
    defend: 100
  };
  u.capture_tower(t);
  t.state = TOWER_STATE.ON_PICKING;
}

/********************** gameloop **************************/


setTimeout(function(){
  LEFT_TIME --;
  var us = UserController.USERS;
  for (var i in us)  {
    us[i].gather_resource();
  }

  if (LEFT_TIME > 0) {
    setTimeout(arguments.callee, 1000);
  } else {
    LEFT_TIME = TOTAL_TIME;
  }
},1000);





/************************ user ****************************/

router.post('/user/update', function(req, res, next) {
  if (!req.body.user_id) res.json({code:-1})
  
  var code = UserController.update(req.body.user_id, req.body);

  res.json({code:code});

});


router.get('/user/list', function(req, res, next) {
  res.json(UserController.USERS);
});


router.get('/user/rank', function(req, res, next) {
  res.json(UserController.ranklist());
});


router.post('/user/info', function(req, res, next) {

  if (!req.body.user_id) res.json({code:-1})
  
  var u = UserController.filter(req.body.user_id);

  if (u==undefined)  res.json({code: 0});
  
  res.json({
    code:     1,
    name:     u.name,
    position: u.position,
    resource: u.resource,
    towers:   u.towers,
  });
});



/*********************** battle ***************************/


router.get('/battle/bounds', function(req, res, next) {
  res.json(TEST_AREA);
});


router.get('/battle/lefttime', function(req, res, next) {
  res.json(LEFT_TIME);
});


/*********************** tower ***************************/

router.post('/tower/info', function(req, res, next) {
  if (!req.body.tower_id) res.json({code:-1})
  
  var t = TowerController.filter(req.body.user_id);

  if (t==undefined)  res.json({code: 0});
  
  res.json({
    code:         1,
    position:     t.position,
    resource:     t.resource,
    tower_state:  t.state,
    owner:        t.owner,
    tech:         t.tech,
  });
});

router.get('/tower/list', function(req, res, next) {
  res.json(TowerController.TOWERS);
});


router.post('/tower/control', function(req, res, next) {
  if (!req.body.user_id || !req.body.tower_id ) 
    res.json({code:-1})

  var u = UserController.filter(req.body.user_id);
  var t = TowerController.filter(req.body.tower_id);

  console.log(u,t)
  if (!u || !t)
    res.json({code:0})

  if (t.state != TOWER_STATE.OUT_CTRL)
    res.json({code:-2})
  
  t.state = TOWER_STATE.ON_PICKING;
  t.owner = {
    user_id: u.id,
    user_name: u.name,
    picking_rate: 10,
    defend: 0
  };
  u.capture_tower(t);

  res.json({code:1});
});


router.post('/tower/defend', function(req, res, next) {
  if (!req.body.user_id || !req.body.tower_id || !req.body.defend_res) 
    res.json({code:-1})

  var u = UserController.filter(req.body.user_id);
  var t = TowerController.filter(req.body.tower_id);

  if (!u || !t)
    res.json({code:0})

  // check owner
  if (u.towers.indexOf(t.id) == -1)
    res.json({code:-3})

  if (t.state != TOWER_STATE.ON_PICKING)
    res.json({code:-2})

  var def = Number(req.body.defend_res);

  if (u.resource < def){
    res.json({code:-5})
  }

  u.resource -= def;
  t.owner.defend += def;

  var tech = req.body.tech;

  if (tech) {
    var allowed = t.spell_tech(tech)
    if (!allowed) {
      res.json({code:-4})
    }
  }

  res.json({code:1})
});


router.post('/tower/attack', function(req, res, next) {
  if (!req.body.user_id || !req.body.tower_id || !req.body.attack_res) 
    res.json({code:-1})

  var u = UserController.filter(req.body.user_id);
  var t = TowerController.filter(req.body.tower_id);

  if (!u || !t)
    res.json({code:0})

  // check owner
  if (u.towers.indexOf(t.id) != -1)
    res.json({code:-3})

  if (t.state != TOWER_STATE.ON_PICKING)
    res.json({code:-2})

  var rate = ATK_DEF_RATE;
  if (t.tech == TECH_TREE.SHEILD){
    rate = SHEILD_RATE;
  }
  
  var rest_res = calculatorLoss(
    Number(req.body.attack_res), 
    Number(t.owner.defend),
    rate
  );

  t.state = TOWER_STATE.ON_FIRE;
  if (rest_res >= 0){
    var war_time = (req.body.attack_res - rest_res) / 50;
    setTimeout(function(){
      war_time --;
      if (war_time > 0) {
        setTimeout(arguments.callee, 1000);
      }else{ // success
        var o = UserController.filter(t.owner.user_id);
        o.lose_tower(t);
        u.capture_tower(t);
        t.owner.user_id = u.id;
        t.owner.user_name = u.name;
        t.state = TOWER_STATE.ON_PICKING;
        t.owner.defend = rest_res;
        t.lose_tech();
        let i = Unit.CalculateIndex(TEST_AREA.slice(0, 2), TEST_AREA.slice(2), 64, t)
        let g = grid[i]
        g.ownerChange(t.owner.user_id)
      }
    },1000);
  }else{
    var war_time = (t.owner.defend + rest_res) / 50;
    setTimeout(function(){
      war_time --;
      if (war_time > 0) {
          setTimeout(arguments.callee, 1000);
      }else{
        t.owner.defend = -rest_res;
        t.state = TOWER_STATE.ON_PICKING;
      }
    },1000);
  }
  res.json({code:1})
});

/*********************************************************/


module.exports = router;
